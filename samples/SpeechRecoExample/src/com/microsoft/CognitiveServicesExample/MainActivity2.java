package com.microsoft.CognitiveServicesExample;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 *
 * Created by sayeedm on 4/18/17.
 */

public class MainActivity2 extends Activity implements BingSpeechListener {

    private Button btn ;
    private TextView result;
    private BingSpeechClient client;
    private boolean isRecognizing = false;

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_2);

        client = BingSpeechClient.getInstance();
        client.initialize(this, this);

        btn = (Button)findViewById(R.id.btn);
        result = (TextView)findViewById(R.id.result);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isRecognizing)
                    client.startRecognition();
                else
                    client.stopRecognition();
            }
        });
    }

    public void onResume(){
        super.onResume();
    }

    public void onDestroy(){
        super.onDestroy();
        if (client != null)
            client.destroy();
    }

    @Override
    public void onRecognitionStarted() {
        isRecognizing = true;
        btn.setText("Stop");
    }

    @Override
    public void onRecognitionCompleted() {
        isRecognizing = false;
        btn.setText("Start");
    }

    @Override
    public void onPartialRecognition(final String text) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                result.setText(text);
            }
        });

    }

    @Override
    public void onRecognitionError() {

    }

    @Override
    public void onRecognitionStopped() {
        isRecognizing = false;
        btn.setText("Start");
    }
}
