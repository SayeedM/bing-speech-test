package com.microsoft.CognitiveServicesExample;

/**
 * Created by sayeedm on 4/18/17.
 */

public interface BingSpeechListener {
    void onRecognitionStarted();
    void onRecognitionCompleted();
    void onPartialRecognition(String text);
    void onRecognitionError();
    void onRecognitionStopped();
}
