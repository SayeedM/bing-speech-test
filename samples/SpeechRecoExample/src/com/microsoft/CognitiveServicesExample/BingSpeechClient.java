package com.microsoft.CognitiveServicesExample;

import android.app.Activity;
import com.microsoft.cognitiveservices.speechrecognition.ISpeechRecognitionServerEvents;
import com.microsoft.cognitiveservices.speechrecognition.MicrophoneRecognitionClient;
import com.microsoft.cognitiveservices.speechrecognition.RecognitionResult;
import com.microsoft.cognitiveservices.speechrecognition.RecognitionStatus;
import com.microsoft.cognitiveservices.speechrecognition.SpeechRecognitionMode;
import com.microsoft.cognitiveservices.speechrecognition.SpeechRecognitionServiceFactory;

/**
 *
 * Created by sayeedm on 4/18/17.
 */

public class BingSpeechClient implements ISpeechRecognitionServerEvents {

    private static BingSpeechClient instance = null;
    private BingSpeechClient(){

    }

    public static BingSpeechClient getInstance(){
        if (instance == null)
            instance = new BingSpeechClient();
        return instance;
    }

    private MicrophoneRecognitionClient micClient = null;
    private Activity context = null;
    private BingSpeechListener listener = null;
    private BingBeep bingBeep;

    public void initialize(Activity context, BingSpeechListener listener){
        if (this.context != null)
            this.context = null;


        this.context = context;
        this.listener = listener;
        bingBeep = BingBeep.getInstance();
        bingBeep.initialize(this.context);

        this.micClient = SpeechRecognitionServiceFactory.createMicrophoneClient(
                this.context,
                SpeechRecognitionMode.ShortPhrase,
                "en-us",
                this,
                this.getPrimaryKey());
    }

    public void destroy(){
        bingBeep.release();
        context = null;
    }


    public void startRecognition(){
        bingBeep.playStart();
        this.micClient.startMicAndRecognition();
        if (listener != null)
            listener.onRecognitionStarted();
    }

    public void stopRecognition(){
        stopRecognition(true);

    }

    private void stopRecognition(boolean isManual){
        this.micClient.endMicAndRecognition();
        if (listener != null) {
            if (isManual) {
                listener.onRecognitionStopped();
            }
        }
    }

    private String getPrimaryKey(){
        return "26f32a87ff344e8585f3cd543f75f8d1";
    }



    @Override
    public void onPartialResponseReceived(String s) {
        System.out.println("Partial " + s);
        if (listener != null)
            listener.onPartialRecognition(s);
    }

    @Override
    public void onFinalResponseReceived(RecognitionResult response) {
        stopRecognition(false);
        bingBeep.playStop();
        if (listener != null)
            listener.onRecognitionCompleted();

        System.out.println("Final " + response.RecognitionStatus.toString());
        if (response.RecognitionStatus == RecognitionStatus.RecognitionSuccess && response.Results != null){
            for (int i = 0; i < response.Results.length; i++) {
                System.out.println(response.Results[i].DisplayText);
            }

        }
    }

    @Override
    public void onIntentReceived(String s) {

    }

    @Override
    public void onError(int i, String s) {
        System.out.println("Error Code : " + i + " message : " + s);
        bingBeep.playStop();
        if (listener != null)
            listener.onRecognitionError();
    }

    @Override
    public void onAudioEvent(boolean b) {

    }
}
