package com.microsoft.CognitiveServicesExample;

import android.content.Context;
import android.media.MediaPlayer;

/**
 * Created by sayeedm on 4/18/17.
 */

public class BingBeep {

    private static BingBeep instance = null;

    private BingBeep(){

    }

    public static BingBeep getInstance(){
        if (instance == null)
            instance = new BingBeep();
        return instance;
    }

    private MediaPlayer mpStart;
    private MediaPlayer mpStop;

    public void initialize(Context context){
        mpStart = MediaPlayer.create(context, R.raw.beep_start);
        mpStop = MediaPlayer.create(context, R.raw.beep_stop);
    }

    public void playStart(){
        mpStart.start();
    }

    public void playStop(){
        mpStop.start();
    }

    public void release(){
        if (mpStart != null){
            if (mpStart.isPlaying())
                mpStart.stop();
            mpStart.reset();
            mpStart.release();
            mpStart = null;
        }


        if (mpStop != null){
            if (mpStop.isPlaying())
                mpStop.stop();
            mpStop.reset();
            mpStop.release();
            mpStop = null;
        }
    }

}
